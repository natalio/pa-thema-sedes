<?php

	/* Template name: Página - Reencontro */

	function custom_style() {
		$file = get_template_directory_uri() . '/custom_reencontro/css/style.css';
		$version = md5( $file );

		wp_enqueue_style( 'reencontro', $file, false, $version );
	}
	add_action( 'wp_enqueue_scripts', 'custom_style', 99999 );

	get_header(); 
	if(have_posts())
		the_post();
?>
<!-- *************************** -->
<!-- ********* Content ********* -->
<!-- *************************** -->
<header class="iasd-institutional-header">
	<div class="container">
		<figcaption>
			<h1><?php single_post_title(); ?></h1>
			<em><?php the_content(); ?></em>
		</figcaption>
	</div>
</header>
<div class="reencontro container">
	<div class="row">
		<div class="col-md-12 iasd-widget"><h1><?php _e( 'Vídeos', 'iasd' ); ?></h1></div>
		<div class="videos">
			<?php 
				$loop = new WP_Query( array( 'post_type' => 'post', 'category_name' => 'reencontro', 'posts_per_page' => 13 ) ); 
				while ( $loop->have_posts() ) : $loop->the_post(); 
			?>
			<div class="video col-md-6 col-sm-6">
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" alt="<?php the_title(); ?>">
					<?php the_post_thumbnail('thumb_132x85', array('class' => 'img-rounded')); ?>
					<span><?php the_title(); ?></span>
				</a>
			</div>
			<?php 
				endwhile; 
				wp_reset_query(); 
			?>
		</div>
	</div>
	<aside class="row">
		<?php do_action('iasd_dynamic_sidebar', 'page'); ?>
	</aside>
</div>

<?php if ( comments_open() ) { ?>
<section class="comments">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php comments_template(); ?>
			</div>
		</div>
	</div>
</section>
<?php } ?>

<!-- *************************** -->
<!-- ******* End Content ******* -->
<!-- *************************** -->

<?php get_footer(); ?>